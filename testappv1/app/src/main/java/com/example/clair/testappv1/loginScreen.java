package com.example.clair.testappv1;

        import android.content.Intent;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.EditText;
        import android.widget.Toast;

public class loginScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginscreen);


    }

    public void passCheck(View a) {
        //compare string taken in passBox to stored string
        String x = ((EditText) findViewById(R.id.editText)).getText().toString();
        if (x.contentEquals("pass")) {
            try {
                Intent intiLandingScreen = new Intent(this, landingScreen.class);
                startActivity(intiLandingScreen);
            } catch (Exception m) {
                m.printStackTrace();
            }
        } else {
            Toast.makeText(this, "incorrect", Toast.LENGTH_SHORT).show();
        }
    }
}