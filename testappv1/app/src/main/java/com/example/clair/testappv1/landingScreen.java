package com.example.clair.testappv1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class landingScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.landingscreen);
    }

    public void gifClick(View b){
        try {
            Intent initGifScreen = new Intent(this, gifScreen.class);
            startActivity(initGifScreen);
        } catch (Exception m) {
            m.printStackTrace();
        }
        /*setContentView(R.layout.gifscreen);
        Intent initGifScreen = new Intent(this, gifscreen.class);
        startActivity(initGifScreen);
*/
    }

    public void databaseClick(View b){
        try {
            Intent initDBScreen = new Intent(this, databaseScreen.class);
            startActivity(initDBScreen);
        } catch (Exception m) {
            m.printStackTrace();
        }

        //setContentView(R.layout.databasescreen);

    }
}

