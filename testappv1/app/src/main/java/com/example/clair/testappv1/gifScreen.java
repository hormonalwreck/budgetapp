package com.example.clair.testappv1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import pl.droidsonroids.gif.GifImageView;

public class gifScreen extends AppCompatActivity {
    ArrayList<Integer> imgs = new ArrayList<>();
    Integer currentI = 0;
    GifImageView gifImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.gifscreen);
        gifImageView = findViewById(R.id.gifView);
        imgs.add(R.drawable.img_1);
        imgs.add(R.drawable.img_2);
        imgs.add(R.drawable.img_3);
        imgs.add(R.drawable.img_4);
        imgs.add(R.drawable.img_5);
        imgs.add(R.drawable.img_7);
        imgs.add(R.drawable.img_8);
        imgs.add(R.drawable.img_9);
        imgs.add(R.drawable.img_10);
        gifImageView.setImageResource(imgs.get(getRandomGif()));
    }

    public void randomGifClick(View x) {
        gifImageView.setImageResource(imgs.get(getRandomGif()));
        }

    public void nextGifClick(View z) {
       // Log.d("currentIVal",currentI.toString());
        if (currentI == 7) {
            currentI = 0;
            gifImageView.setImageResource(imgs.get(currentI));
        } else {
            gifImageView.setImageResource(imgs.get(currentI+1));
            currentI++;
        }
    }

    public void prevGifClick(View c) {
        Log.d("currentIVal",currentI.toString());
        if (currentI == 0) {
            currentI = 7;
            gifImageView.setImageResource(imgs.get(currentI));
        } else {
            gifImageView.setImageResource(imgs.get(currentI-1));
            currentI--;
        }
    }

    public int getRandomGif(){
        Random n = new Random();
        Integer randI;
        do {
            randI = n.nextInt(imgs.size());
        }
        while(randI == currentI);
        currentI = randI;
        return randI;
    }
}
