package com.example.clair.testappv1;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class databaseScreen extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getJSON("http://marianowinska.com/getData.php");
        //getJSON("file:\\\\C:\\mock\\getData.php");
    }

    public void makeThatTableWork(String[] id, String[] restaurants, String[] foodname){
        //setContentView(R.layout.databasescreen);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        //relativeLayout.setBackgroundColor(Color.BLACK);
        TableLayout tableLayout = getTable();
        TableRow tableHeading = getTableHeading();


        tableLayout.addView(tableHeading);
        for (int x = 0; x < 3; x++){
            TableRow tableRow1 = generateTableRow(id[x], restaurants[x], foodname[x]);
            tableLayout.addView(tableRow1);
        }

        RelativeLayout.LayoutParams centerTableParameters
                = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        centerTableParameters.addRule(RelativeLayout.CENTER_HORIZONTAL);

        relativeLayout.addView(tableLayout, centerTableParameters);
        setContentView(relativeLayout);

    }

    public TableLayout getTable(){
        TableLayout tableLayout = new TableLayout(this);
        //tableLayout.setMinimumWidth(1920);
       /*TableLayout.LayoutParams p = new TableLayout.LayoutParams();

        p.leftMargin = 10;
        p.bottomMargin = 100;
        p.weight = 200;
        tableLayout.setLayoutParams(p);*/
        return tableLayout;
    }

    public TableRow getTableHeading(){
        TableRow tableRow = new TableRow(this);
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        //layoutParams.column = 0;
        layoutParams.span = 3;

        TextView tableHeading = new TextView(this);
        tableHeading.setText("Restaurant List");
        tableHeading.setTextSize(40);
        tableHeading.setGravity(Gravity.CENTER);

        tableRow.addView(tableHeading,layoutParams);
        return tableRow;
    }

    public TableRow generateTableRow(String first, String second, String third){
        TableRow tableRow = new TableRow(this);

        Button button1 = new Button(this);
        button1.setText(third);
        button1.setTextSize(20);
        tableRow.addView(button1);

        TextView firstColumn = new TextView(this);
        firstColumn.setPadding(40,0,40,0);
        firstColumn.setText(first);
        firstColumn.setTextSize(20);
        //tableRow.addView(firstColumn);


        TextView secondColumn = new TextView(this);
        secondColumn.setPadding(40,0,40,0);
        secondColumn.setText(second);
        secondColumn.setTextSize(20);
        secondColumn.setAllCaps(true);
       // tableRow.addView(secondColumn);

        /*final EditText editTextName2 = new EditText(this);
        editTextName2.setImeOptions(EditorInfo.IME_ACTION_NEXT);  //Investigate what this is?
        editTextName2.setHint(second);
        editTextName2.setInputType(InputType.TYPE_CLASS_TEXT);  //Investigate what this is?
        tableRow.addView(editTextName2);*/

        TextView thirdColumn = new TextView(this);
        thirdColumn.setPadding(40,0,40,0);
        thirdColumn.setText(second);
        thirdColumn.setTextSize(20);
        tableRow.addView(thirdColumn);

        return tableRow;
    }



    private void loadIntoListView(String json) throws JSONException {
        //creating a json array from the json string
        String json2 = "[{\"id\":\"1\",\"restaurantname\":\"Pizza Hut\",\"foodname\":\"Margherita\"},{\"id\":\"2\",\"restaurantname\":\"McDonalds\",\"foodname\":\"Big Mac\"},{\"id\":\"3\",\"restaurantname\":\"Nandos\",\"foodname\":\"Chicken\"}]";
        JSONArray jsonArray = new JSONArray(json);


        //creating a string array for listview
        String[] id = new String[jsonArray.length()];
        String[] restaurants = new String[jsonArray.length()];
        String[] foodname = new String[jsonArray.length()];

        //looping through all the elements in json array
        for (int i = 0; i < jsonArray.length(); i++) {

            //getting json object from the json array
            JSONObject obj = jsonArray.getJSONObject(i);

            //getting the name from the json object and putting it inside string array
            id[i] = obj.getString("id");
            Log.d("Message: ",id[i]);
            restaurants[i] = obj.getString("restaurantname");
            foodname[i] = obj.getString("foodname");
        }

        makeThatTableWork(id,restaurants,foodname);

        //the array adapter to load data into list
       // ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, restaurants);


        //attaching adapter to listview
        //listView = (ListView) findViewById(R.id.listView);
        //listView.setAdapter(arrayAdapter);
    }


    //this method is actually fetching the json string
    private void getJSON(final String urlWebService) {
        /*
         * As fetching the json string is a network operation
         * And we cannot perform a network operation in main thread
         * so we need an AsyncTask
         * The constrains defined here are
         * Void -> We are not passing anything
         * Void -> Nothing at progress update as well
         * String -> After completion it should return a string and it will be the json string
         * */
        class GetJSON extends AsyncTask<Void, Void, String> {

            //this method will be called before execution
            //you can display a progress bar or something
            //so that user can understand that he should wait
            //as network operation may take some time
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            //this method will be called after execution
            //so here we are displaying a toast with the json string
            @Override
            protected void onPostExecute(String s) {
                //super.onPostExecute(s);
                //Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
                try {
                    loadIntoListView(s);



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            //in this method we are fetching the json string
            @Override
            protected String doInBackground(Void... voids) {

                try {
                    //creating a URL
                    URL url = new URL(urlWebService);

                    //Opening the URL using HttpURLConnection
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();

                    //StringBuilder object to read the string from the service
                    StringBuilder sb = new StringBuilder();

                    //We will use a buffered reader to read the string from service
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    //A simple string to read values from each line
                    String json;

                    //reading until we don't find null
                    while ((json = bufferedReader.readLine()) != null) {

                        //appending it to string builder
                        sb.append(json + "\n");
                    }
                    
                    //finally returning the read string
                    return sb.toString().trim();
                } catch (Exception e) {
                    return null;
                }

            }
        }

        //creating asynctask object and executing it
        GetJSON getJSON = new GetJSON();
        getJSON.execute();
    }

}
